#version 130

in vec3 position;
in vec2 tex;
out vec2 frag_tex;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main(void) {
    frag_tex = tex;
    gl_Position = projection * view * model * vec4(position.xyz, 1.0);
}
