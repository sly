;;; Sly
;;; Copyright (C) 2014, 2015 David Thompson <davet@gnu.org>
;;;
;;; Sly is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Sly is distributed in the hope that it will be useful, but WITHOUT
;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;; License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Development environment for GNU Guix.
;;
;; To setup the development environment, run the following:
;;
;;    guix environment -l guix.scm
;;    ./bootstrap && ./configure
;;
;; To build the development snapshot, run:
;;
;;    guix build -f guix.scm
;;
;; To install the development snapshot, run:
;;
;;    guix install -f guix.scm
;;
;;; Code:

(use-modules (ice-9 match)
             (srfi srfi-1)
             (guix gexp)
             (guix packages)
             (guix licenses)
             (guix git-download)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (gnu packages guile)
             (gnu packages gl)
             (gnu packages sdl)
             (gnu packages maths)
             (gnu packages image))

(define use-guile-2.2?
  (string? (getenv "SLY_GUILE_NEXT")))

(define (package-with-guile p guile)
  (package
    (inherit p)
    (inputs
     (map (match-lambda
            (("guile" _)
             `("guile" ,guile))
            (input input))
          (package-inputs p)))))

(define (package-with-guile-next-maybe p)
  (if use-guile-2.2?
      (package-with-guile p guile-next)
      p))

(define guile-sdl2
  (package
   (name "guile-sdl2")
   (version "0.1.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://dthompson.us/guile-sdl2.git")
                  (commit "55c4eef")))
            (sha256
             (base32
              "1yckf6n2iwp5l3yz4y0rks5qbylnmfnpcxmlwl74aqyqfxd586ix"))))
   (build-system gnu-build-system)
   (arguments
    '(#:configure-flags
      (list (string-append "--with-libsdl2-prefix="
                           (assoc-ref %build-inputs "sdl2"))
            (string-append "--with-libsdl2-image-prefix="
                           (assoc-ref %build-inputs "sdl2-image"))
            (string-append "--with-libsdl2-ttf-prefix="
                           (assoc-ref %build-inputs "sdl2-ttf"))
            (string-append "--with-libsdl2-mixer-prefix="
                           (assoc-ref %build-inputs "sdl2-mixer")))
      #:make-flags '("GUILE_AUTO_COMPILE=0")
      #:phases
      (modify-phases %standard-phases
        (add-after 'unpack 'bootstrap
          (lambda _ (zero? (system* "sh" "bootstrap"))))
        (add-after 'configure 'patch-makefile
          (lambda _
            ;; Install compiled Guile files in the expected place.
            (substitute* '("Makefile")
              (("^godir = .*$")
               "godir = $(moddir)\n")))))))
   (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("pkg-config" ,pkg-config)))
   (inputs
    `(("guile" ,guile-2.0)
      ("sdl2" ,sdl2)
      ("sdl2-image" ,sdl2-image)
      ("sdl2-mixer" ,sdl2-mixer)
      ("sdl2-ttf" ,sdl2-ttf)))
   (synopsis "Guile bindings for SDL2")
   (description "Guile-sdl2 provides pure Guile Scheme bindings to the
SDL2 C shared library via the foreign function interface.")
   (home-page "https://git.dthompson.us/guile-sdl2.git")
   (license lgpl3+)))

(define sly
  (package
    (name "sly")
    (version "0.2.0")
    (source (local-file "." #:recursive? #t))
    (build-system gnu-build-system)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'bootstrap
           (lambda _ (zero? (system* "sh" "bootstrap")))))))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("autoconf" ,autoconf)
       ("automake" ,automake)
       ("texinfo" ,texinfo)))
    (propagated-inputs
     `(("guile-sdl2" ,(package-with-guile-next-maybe guile-sdl2))
       ("guile-opengl" ,(package-with-guile-next-maybe guile-opengl))))
    (inputs
     `(("guile" ,guile-2.0)
       ("gsl" ,gsl)
       ("mesa" ,mesa)))
    (synopsis "2D/3D game engine for GNU Guile")
    (description "Sly is a 2D/3D game engine written in Guile Scheme.
Sly differs from most game engines in that it emphasizes functional
reactive programming and live coding.")
    (home-page "http://dthompson.us/pages/software/sly.html")
    (license gpl3+)))

(package-with-guile-next-maybe sly)
