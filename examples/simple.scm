;;; Sly
;;; Copyright (C) 2013, 2014, 2015 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (sly))

(load "common.scm")

(define-signal scene
  (signal-let ((sprite (on-start (load-sprite "images/p1_front.png")
                                 null-sprite)))
    (with-camera (2d-camera #:area (make-rect 0 0 640 480))
      (move (vector2 320 240)
            (render-sprite sprite)))))

(with-window (make-window #:title "Simple Sprite Demo")
  (run-game-loop scene))

;;; Local Variables:
;;; compile-command: "../pre-inst-env guile simple.scm"
;;; End:
