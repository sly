;;; Sly
;;; Copyright (C) 2016 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (sly)
             (sly render particles)
             (sly render sprite-batch)
             (sly render utils))

(load "common.scm")

(define (particle-position n time life-span)
  (polar2 (+ (* time 2) (* (modulo n 17) (/ time 8)))
          (+ (/ (sin (* n n)) 7) pi/2)))

(define-signal texture
  (on-start (load-texture "images/bullet.png")
            null-texture))

(define-signal particle-system
  (signal-let ((texture texture))
    (make-particle-system #:texture texture
                          #:emit-interval 2
                          #:emit-count 16
                          #:life-span 60
                          #:renderer (make-simple-particle-renderer
                                      particle-position))))

(define-signal batch
  (on-start (make-sprite-batch 4096)))

(define-signal scene
  (signal-let ((particle-system particle-system)
               (batch batch)
               (time (signal-timer)))
    (if batch
        (with-camera (2d-camera #:area (make-rect 0 0 640 480))
          (with-blend-mode (make-blend-mode 'src-alpha 'one)
            (move (vector2 320 180)
                  (render-particles particle-system batch time))))
        render-nothing)))

(with-window (make-window #:title "Particles")
  (run-game-loop scene))

;;; Local Variables:
;;; compile-command: "../pre-inst-env guile particles.scm"
;;; End:
