;;; Sly
;;; Copyright (C) 2015 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (sly game)
             (sly window)
             (sly utils)
             (sly signal)
             (sly math rect)
             (sly math vector)
             (sly render)
             (sly render camera)
             (sly render color)
             (sly render framebuffer)
             (sly render shader)
             (sly render sprite)
             (sly render texture))

(load "common.scm")

(define inner-camera
  (2d-camera #:area (make-rect 0 0 320 240)))

(define outer-camera
  (2d-camera #:area (make-rect 0 0 640 480)))

(define-signal framebuffer
  (on-start (make-framebuffer 320 240)))

(define-signal outer-sprite
  (signal-map-maybe (lambda (framebuffer)
                      (make-sprite (framebuffer-texture framebuffer)
                                   #:anchor 'bottom-left))
                    framebuffer))

(define-signal player-sprite
  (on-start (load-sprite "images/p1_front.png")))

(define-signal scene
  (signal-let ((framebuffer framebuffer)
               (outer-sprite outer-sprite)
               (inner-sprite player-sprite))
    (if (and framebuffer outer-sprite inner-sprite)
        (render-begin
         (with-framebuffer framebuffer
           (with-camera inner-camera
             (move (vector2 160 120) (render-sprite inner-sprite))))
         (with-camera outer-camera
           (scale 2 (render-sprite outer-sprite))))
        render-nothing)))

(with-window (make-window #:title "Simple Sprite Demo")
  (run-game-loop scene))

;;; Local Variables:
;;; compile-command: "../pre-inst-env guile simple.scm"
;;; End:
