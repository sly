;;; Sly
;;; Copyright © 2016 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (sly)
             (sly render sprite-batch)
             (srfi srfi-1))

(load "common.scm")

(define (make-sprite-rects size texture)
  (let ((width (texture-width texture))
        (height (texture-height texture)))
    (list-tabulate size
                   (lambda _
                     (make-rect (- (random 640) 32) (- (random 480) 32)
                                width height)))))

(define (render-sprites batch texture rects)
  (lambda (gfx)
    (with-sprite-batch batch gfx
      (for-each (lambda (rect)
                  (sprite-batch-add! batch gfx texture rect))
                rects))))

(define-signal size 1000)

(define-signal sprites
  (let ((texture (on-start (load-texture "images/p1_front.png"))))
    (signal-map-maybe render-sprites
                      (signal-map (lambda (size started?)
                                    (and started? (make-sprite-batch size)))
                                  size game-started?)
                      texture
                      (signal-map-maybe make-sprite-rects size texture))))

(define-signal scene
  (signal-let ((sprites sprites))
    (if sprites
        (with-camera (2d-camera #:area (make-rect 0 0 640 480))
          sprites)
        render-nothing)))

(with-window (make-window #:title "Sprite Batch")
  (run-game-loop scene))

;;; Local Variables:
;;; compile-command: "../pre-inst-env guile sprite-batch.scm"
;;; End:
