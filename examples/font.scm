;;; Sly
;;; Copyright (C) 2013, 2014 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (sly game)
             (sly fps)
             (sly signal)
             (sly window)
             (sly math rect)
             (sly math vector)
             (sly render)
             (sly render camera)
             (sly render color)
             (sly render font)
             (sly render sprite)
             (sly render texture)
             (sly input mouse))

(load "common.scm")

(define camera (2d-camera #:area (make-rect 0 0 640 480)))

(define (render-messages font fps pos)
  (render-begin
   (move (vector2 320 240)
         (render-sprite
          (make-label font "The quick brown fox jumped over the lazy dog."
                      #:anchor 'center)))
   (let ((text (format #f "FPS: ~d" fps)))
     (move (vector2 0 480)
           (render-sprite (make-label font text))))
   (let ((text (format #f "Mouse: (~d, ~d)" (vx pos) (vy pos))))
     (move (vector2 0 460)
           (render-sprite (make-label font text))))))

(define-signal font (on-start (load-default-font 18)))

(define-signal scene
  (signal-let ((fps fps)
               (mouse-position (signal-throttle 10 mouse-position))
               (font font))
    (with-camera camera
      (if font
          (render-messages font fps mouse-position)
          render-nothing))))

(with-window (make-window #:title "Fonts")
  (enable-fonts)
  (run-game-loop scene))

;;; Local Variables:
;;; compile-command: "../pre-inst-env guile font.scm"
;;; End:
