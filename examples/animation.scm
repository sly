;;; Sly
;;; Copyright (C) 2013, 2014 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (srfi srfi-43)
             (sly utils)
             (sly game)
             (sly window)
             (sly signal)
             (sly math)
             (sly math rect)
             ((sly math transform) #:prefix t:)
             (sly math tween)
             (sly math vector)
             (sly render)
             (sly render camera)
             (sly render sprite)
             (sly render tileset))

(load "common.scm")

(define sprite* (memoize make-sprite))
(define move* (memoize move))

(define (make-walk-cycle tiles)
  (list->vector
   (map (lambda (id)
          (sprite* (tileset-ref tiles id)))
        '(19 20 21 22 23 24 25 26))))

(define position-tween
  (tween vlerp (compose ease-linear ease-loop)
         (vector2 640 240) (vector2 0 240) 90))

(define (make-frame-tween walk-cycle)
  (if (vector-empty? walk-cycle)
      (const 0)
      (let* ((frame-count (vector-length walk-cycle))
             (frame-rate (/ 60 frame-count)))
        (tween (compose floor lerp) (compose ease-linear ease-loop)
               0 frame-count (* frame-count frame-rate)))))

(define camera (2d-camera #:area (make-rect 0 0 640 480)))

(define (move/interpolate new old renderer)
  (lambda (gfx)
    (graphics-model-view-excursion gfx
      (lambda (gfx)
        (let ((v (vlerp old new (graphics-alpha gfx))))
          (graphics-model-view-mul! gfx (t:translate v))
          (renderer gfx))))))

(define (render time walk-cycle frame-tween)
  (let* ((frame (vector-ref walk-cycle (frame-tween time))))
    (with-camera camera
      (move/interpolate (position-tween time)
                        (position-tween (1- time))
                        (render-sprite frame)))))

(define-signal walk-cycle
  (signal-map-maybe make-walk-cycle
                    (on-start (load-tileset "images/princess.png" 64 64))))

(define-signal time (signal-timer))

(define-signal frame-tween
  (signal-map-maybe make-frame-tween walk-cycle))

(define-signal scene
  (signal-map (lambda (render)
                (or render render-nothing))
              (signal-map-maybe render time walk-cycle frame-tween)))

(with-window (make-window #:title "Animation")
  (run-game-loop scene))

;;; Local Variables:
;;; compile-command: "../pre-inst-env guile animation.scm"
;;; End:
