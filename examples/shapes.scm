;;; Sly
;;; Copyright (C) 2015 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (srfi srfi-26)
             (sly game)
             (sly window)
             (sly utils)
             (sly signal)
             (sly math rect)
             (sly math vector)
             (sly render)
             (sly render camera)
             (sly render color)
             (sly render shader)
             (sly render shape)
             (sly render sprite)
             (sly render texture))

(load "common.scm")

(define-signal crate-texture
  (on-start (load-texture "images/crate.png")))

(define-signal sky-background
  (on-start (load-sprite "images/country-sky.png")))

(define-signal forest-background
  (on-start (load-sprite "images/country-trees.png")))

(define (make-background sky forest)
  (scale 1/10
         (move (vector3 0 0 -10)
               (render-begin
                (render-sprite sky)
                (render-sprite forest)))))

(define-signal background
  (signal-map-maybe make-background
                    sky-background
                    forest-background))

(define-signal unit-cube
  (signal-map-maybe (cut make-cube 1 #:texture <>)
                    crate-texture))

(define (make-cubes unit-cube)
  (move (vector3 0 0 -3)
        (render-begin
         (move (vector3 2 0 0) unit-cube)
         unit-cube
         (move (vector3 -2 0 0) unit-cube))))

(define-signal cubes
  (signal-map-maybe make-cubes unit-cube))

(define camera
  (3d-camera #:area (make-rect 0 0 640 480)))

(define-signal scene
  (signal-let ((rotation (signal-map (lambda (x) (/ x 48))
                                     (signal-timer)))
               (background background)
               (cubes cubes))
    (if (and background cubes)
        (with-camera camera
          (render-begin
           background
           ;; Spinnin' cubes!
           (with-depth-test #t
             (rotate-x (* rotation 2)
                       (rotate-y rotation cubes)))))
        render-nothing)))

(with-window (make-window #:title "Shapes!")
  (run-game-loop scene))

;;; Local Variables:
;;; compile-command: "../pre-inst-env guile shapes.scm"
;;; End:
