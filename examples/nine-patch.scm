;;; Sly
;;; Copyright (C) 2016 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (sly)
             (sly render nine-patch))

(load "common.scm")

(define-signal dialog
  (on-start
   (render-nine-patch #:texture (load-texture "images/dialog.png")
                      #:width 512
                      #:height 256
                      #:margin 8)
   render-nothing))

(define-signal scene
  (signal-let ((dialog dialog))
    (with-camera (2d-camera #:area (make-rect 0 0 640 480))
      (move (vector2 320 240) dialog))))

(with-window (make-window #:title "9-patch")
  (run-game-loop scene))

;;; Local Variables:
;;; compile-command: "../pre-inst-env guile nine-patch.scm"
;;; End:
