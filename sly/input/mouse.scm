;;; Sly
;;; Copyright (C) 2013, 2014 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Mouse signals.
;;
;;; Code:

(define-module (sly input mouse)
  #:use-module (ice-9 match)
  #:use-module ((sdl2 events) #:prefix sdl2:)
  #:use-module (sly window)
  #:use-module (sly event)
  #:use-module (sly signal)
  #:use-module (sly math vector)
  #:export (mouse-move-hook
            mouse-press-hook
            mouse-click-hook
            mouse-x
            mouse-y
            mouse-position
            mouse-last-down
            mouse-last-up
            mouse-down?))

(define mouse-move-hook (make-hook 1))

(register-event-handler
 'mouse-motion
 (lambda (e)
   (run-hook mouse-move-hook
             (vector2 (sdl2:mouse-motion-event-x e)
                      (sdl2:mouse-motion-event-y e)))))

(define-signal mouse-position
  (hook->signal mouse-move-hook
                (vector2 0 0)
                ;; Sly uses the bottom-left as the origin, so invert
                ;; the y-axis for convenience.
                (match-lambda
                  (($ <vector2> x y)
                   (vector2 x (- (signal-ref window-height) y))))))

(define-signal mouse-x (signal-map vx mouse-position))
(define-signal mouse-y (signal-map vy mouse-position))

(define mouse-press-hook (make-hook 2))

(register-event-handler
 'mouse-button-down
 (lambda (e)
   (run-hook mouse-press-hook
             (sdl2:mouse-button-event-button e)
             (vector2 (sdl2:mouse-button-event-x e)
                      (sdl2:mouse-button-event-y e)))))

(define-signal mouse-last-down
  (hook->signal mouse-press-hook
                'none
                (lambda (button position) button)))

(define mouse-click-hook (make-hook 2))

(register-event-handler
 'mouse-button-up
 (lambda (e)
   (run-hook mouse-click-hook
             (sdl2:mouse-button-event-button e)
             (vector2 (sdl2:mouse-button-event-x e)
                      (sdl2:mouse-button-event-y e)))))

(define-signal mouse-last-up
  (hook->signal mouse-click-hook
                'none
                (lambda (button position) button)))

(define (mouse-down? button)
  "Create a signal for the state of BUTTON. Value is #t when mouse
button is pressed or #f otherwise."
  (define (same-button? other-button)
    (eq? button other-button))

  (define (button-filter value signal)
    (signal-constant value (signal-filter same-button? #f signal)))

  (signal-merge (button-filter #f mouse-last-up)
                (button-filter #t mouse-last-down)))
