;;; Sly
;;; Copyright (C) 2014, 2015, 2016 David Thompson <davet@gnu.org>
;;;
;;; Sly is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Sly is distributed in the hope that it will be useful, but WITHOUT
;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;;; License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Cameras.
;;
;;; Code:

(define-module (sly render camera)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:use-module (gl)
  #:use-module (gl low-level)
  #:use-module (gl enums)
  #:use-module (sly wrappers gl)
  #:use-module (sly utils)
  #:use-module (sly render)
  #:use-module (sly render color)
  #:use-module (sly render viewport)
  #:use-module (sly math rect)
  #:use-module (sly math transform)
  #:use-module (sly math vector)
  #:export (make-camera
            camera?
            camera-projection
            camera-location
            camera-viewport
            with-camera
            2d-camera
            3d-camera))

(define-record-type <camera>
  (make-camera projection location viewport)
  camera?
  (projection camera-projection)
  (location camera-location)
  (viewport camera-viewport))

(define (with-camera camera renderer)
  (projection-excursion
   (render-begin
    projection-identity
    (projection-mul (camera-projection camera))
    (view-excursion
     (render-begin
      view-identity
      (view-mul (translate (camera-location camera)))
      (with-viewport (camera-viewport camera)
        (render-begin
         clear-screen
         renderer)))))))

(define* (2d-camera #:key (z-near 0) (z-far 1) (area (make-rect 0 0 640 480))
                    (clear-color black) (clear-flags %standard-clear-flags)
                    (location (vector2 0 0)))
  "Create a camera that uses an orthographic (2D) projection that
spans AREA in the framebuffer. By default, this area is (0, 0)
-> (640, 480) pixels.  Z-axis clipping planes Z-NEAR and Z-FAR may be
specified but default to 0 and 1, respectively.  CLEAR-COLOR specifies
the background color used when clearing the screen.  Black is used by
default.  CLEAR-FLAGS specifies the buffers that are cleared when the
camera is applied.  The color and depth buffers are cleared by
default.  LOCATION specifies the position of the camera in the scene.
The origin is used as the default location."
  (let ((viewport (make-viewport area #:clear-color clear-color
                                 #:clear-flags clear-flags))
        (projection (orthographic-projection 0 (rect-width area)
                                             (rect-height area) 0
                                             z-near z-far)))
    (make-camera projection location viewport)))

(define* (3d-camera #:key (z-near 1) (z-far 10) (area (make-rect 0 0 640 480))
                    (field-of-vision 90) (aspect-ratio (/ 4 3))
                    (location (vector3 0 0 0)) (clear-color black)
                    (clear-flags %standard-clear-flags))
  (let ((viewport (make-viewport area #:clear-color clear-color
                                 #:clear-flags clear-flags))
        (projection (perspective-projection field-of-vision aspect-ratio
                                            z-near z-far)))
    (make-camera projection location viewport)))
