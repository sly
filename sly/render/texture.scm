;;; Sly
;;; Copyright (C) 2013, 2014 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Textures and texture regions are high level wrappers over OpenGL
;; textures.
;;
;;; Code:

(define-module (sly render texture)
  #:use-module (ice-9 match)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-9)
  #:use-module (gl)
  #:use-module (gl low-level)
  #:use-module (gl contrib packed-struct)
  #:use-module (sdl2 image)
  #:use-module (sdl2 surface)
  #:use-module (sly guardian)
  #:use-module (sly utils)
  #:use-module (sly render color)
  #:use-module (sly math vector)
  #:use-module (sly wrappers gl)
  #:export (make-texture
            make-texture-region
            load-texture
            texture?
            texture-region?
            texture-null?
            texture-id
            texture-parent
            texture-width
            texture-height
            texture-s1
            texture-t1
            texture-s2
            texture-t2
            null-texture
            anchor-texture
            apply-texture))

;;;
;;; Textures
;;;

;; The <texture> object is a simple wrapper around an OpenGL texture
;; id.
(define-record-type <texture>
  (%make-texture id parent width height s1 t1 s2 t2)
  texture?
  (id texture-id)
  (parent texture-parent)
  (width texture-width)
  (height texture-height)
  (s1 texture-s1)
  (t1 texture-t1)
  (s2 texture-s2)
  (t2 texture-t2))

(define null-texture (%make-texture 0 #f 0 0 0 0 0 0))

(define (texture-null? texture)
  "Return #t if TEXTURE is the null texture."
  (eq? texture null-texture))

(define (texture-region? texture)
  "Return #t if TEXTURE has a parent texture."
  (texture? (texture-parent texture)))

(define (make-texture id parent width height s1 t1 s2 t2)
  "Create a new texture object. ID is the OpenGL texture id. PARENT is
a texture object (if this texture only represents a region of another
texture) or #f. WIDTH and HEIGHT are the texture dimensions in
pixels. S1, T1, S2, and T2 are the OpenGL texture coordinates
representing the area of the texture that will be rendered."
  (guard (%make-texture id parent width height s1 t1 s2 t2)))

(define (make-texture-region texture x y width height)
  "Creates new texture region object. TEXTURE is the region's parent
texture. X, Y, WIDTH, and HEIGHT represent the region of the texture
that will be rendered, in pixels."
  (let* ((w (texture-width texture))
         (h (texture-height texture)))
    (make-texture (texture-id texture)
                  texture
                  width
                  height
                  (/ x w)
                  (/ y h)
                  (/ (+ x width) w)
                  (/ (+ y height) h))))

(define (free-texture texture)
  ;; Do not reap texture regions.
  (unless (texture-region? texture)
    (gl-delete-texture (texture-id texture))))

(register-finalizer texture? free-texture)

(define (apply-texture texture)
  (gl-enable (enable-cap texture-2d))
  (glBindTexture (texture-target texture-2d)
                 (texture-id texture)))

(define* (bytevector->texture pixels width height min-filter mag-filter
                              #:optional (format (pixel-format rgba)))
  "Translate the bytevector PIXELS into an OpenGL texture with
dimensions WIDTHxHEIGHT where each pixel corresponds to the given
OpenGL pixel FORMAT.  The generated textured uses MIN-FILTER for
downscaling and MAG-FILTER for upscaling."
  (let ((texture-id (gl-generate-texture)))
    (with-gl-bind-texture (texture-target texture-2d) texture-id
      (gl-texture-parameter (texture-target texture-2d)
                            (texture-parameter-name texture-min-filter)
                            (match min-filter
                              ('nearest (texture-min-filter nearest))
                              ('linear (texture-min-filter linear))))
      (gl-texture-parameter (texture-target texture-2d)
                            (texture-parameter-name texture-mag-filter)
                            (match mag-filter
                              ('nearest (texture-mag-filter nearest))
                              ('linear (texture-mag-filter linear))))
      (gl-texture-image-2d (texture-target texture-2d)
                           0 (pixel-format rgba) width height 0 format
                           (color-pointer-type unsigned-byte)
                           pixels))
    (make-texture texture-id #f width height 0 0 1 1)))

(define (flip-pixels-vertically pixels width height)
  "Create a new bytevector that reverses the rows in PIXELS, a WIDTH x
HEIGHT, 32 bit color bytevector."
  (let ((buffer (make-u8vector (bytevector-length pixels)))
        (row-width (* width 4))) ; assuming 32 bit color
    (let loop ((y 0))
      (when (< y height)
        (let* ((y* (- height y 1))
               (source-start (* y row-width))
               (target-start (* y* row-width)))
          (bytevector-copy! pixels source-start buffer target-start row-width)
          (loop (1+ y)))))
    buffer))

(define (surface->texture surface min-filter mag-filter)
  "Convert SURFACE, an SDL2 surface object, into a texture that uses
the given MIN-FILTER and MAG-FILTER."
  ;; Convert to 32 bit RGBA color.
  (call-with-surface (convert-surface-format surface 'abgr8888)
    (lambda (surface)
      (let* ((width (surface-width surface))
             (height (surface-height surface))
             ;; OpenGL textures use the bottom-left corner as the
             ;; origin, whereas SDL uses the top-left, so the rows
             ;; of pixels must be reversed before creating a
             ;; texture from them.
             (pixels (flip-pixels-vertically (surface-pixels surface)
                                             width height)))
        (bytevector->texture pixels width height
                             min-filter mag-filter)))))

(define* (load-texture file #:optional #:key
                       (min-filter 'nearest) (mag-filter 'nearest))
  "Load a texture from an image in FILE.  MIN-FILTER and MAG-FILTER
describe the method that should be used for minification and
magnification.  Valid values are 'nearest and 'linear.  By default,
'nearest is used."
  (call-with-surface (load-image file)
    (lambda (surface)
      (surface->texture surface min-filter mag-filter))))

(define (anchor-texture texture anchor)
  "Translate ANCHOR into a vector that represents the desired central
point for TEXTURE.  Valid values for ANCHOR are: 'center, 'top-left,
'top-right, 'bottom-left, 'bottom-right, 'top-center, 'bottom-center,
or any 2D vector.  Passing a 2D vector will simply cause the same
vector to be returned."
  (let ((w (texture-width texture))
        (h (texture-height texture)))
    (anchor-vector w h anchor)))
