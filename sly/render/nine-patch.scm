;;; Sly
;;; Copyright (C) 2016 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; 9-patch sprites.
;;
;;; Code:

(define-module (sly render nine-patch)
  #:use-module (ice-9 match)
  #:use-module (sly render)
  #:use-module (sly render mesh)
  #:use-module (sly render texture)
  #:use-module (sly math vector)
  #:export (render-nine-patch))

(define* (render-nine-patch #:key texture (width 0) (height 0) (margin 0)
                            (top-margin margin) (bottom-margin margin)
                            (left-margin margin) (right-margin margin)
                            (anchor 'center))
  "Create a renderer that draws a 9-patch sprite.  A 9-patch sprite
renders TEXTURE as a WIDTH x HEIGHT rectangle whose stretchable areas
are defined by the given margin measurements.  The corners are never
stretched, the left and right edges may be stretched vertically, the
top and bottom edges may be stretched horizontally, and the center may
be stretched in both directions.  This rendering technique is
particularly well suited for resizable windows and buttons in
graphical user interfaces.

MARGIN specifies the margin size for all sides of the 9-patch.  To
make margins of differing sizes, the TOP-MARGIN, BOTTOM-MARGIN,
LEFT-MARGIN, and RIGHT-MARGIN arguments may be used.

ANCHOR defines the origin point for the rendered 9-patch, which is
'center' by default."
  (let* ((anchor (anchor-vector width height anchor))
         (border-x1 (- (vx anchor)))
         (border-y1 (- (vy anchor)))
         (border-x2 (+ border-x1 width))
         (border-y2 (+ border-y1 height))
         (fill-x1 (+ border-x1 left-margin))
         (fill-y1 (+ border-y1 bottom-margin))
         (fill-x2 (- border-x2 right-margin))
         (fill-y2 (- border-y2 top-margin))
         (border-s1 (texture-s1 texture))
         (border-t1 (texture-t1 texture))
         (border-s2 (texture-s2 texture))
         (border-t2 (texture-t2 texture))
         (tw (texture-width texture))
         (th (texture-height texture))
         (fill-s1 (/ left-margin tw))
         (fill-t1 (/ bottom-margin th))
         (fill-s2 (/ (- tw right-margin) tw))
         (fill-t2 (/ (- th top-margin) th))
         (mesh (build-mesh #(0 4 5 0 5 1          ; bottom-left
                               1 5 6 1 6 2        ; bottom-center
                               2 6 7 2 7 3        ; bottom-right
                               4 8 9 4 9 5        ; center-left
                               5 9 10 5 10 6      ; center
                               6 10 11 6 11 7     ; center-right
                               8 12 13 8 13 9     ; top-left
                               9 13 14 9 14 10    ; top-center
                               10 14 15 10 15 11) ; top-right
                           (vector
                            (vector3 border-x1 border-y1 0)
                            (vector3 fill-x1 border-y1 0)
                            (vector3 fill-x2 border-y1 0)
                            (vector3 border-x2 border-y1 0)

                            (vector3 border-x1 fill-y1 0)
                            (vector3 fill-x1 fill-y1 0)
                            (vector3 fill-x2 fill-y1 0)
                            (vector3 border-x2 fill-y1 0)

                            (vector3 border-x1 fill-y2 0)
                            (vector3 fill-x1 fill-y2 0)
                            (vector3 fill-x2 fill-y2 0)
                            (vector3 border-x2 fill-y2 0)

                            (vector3 border-x1 border-y2 0)
                            (vector3 fill-x1 border-y2 0)
                            (vector3 fill-x2 border-y2 0)
                            (vector3 border-x2 border-y2 0))
                           (vector
                            (vector3 border-s1 border-t1 0)
                            (vector3 fill-s1 border-t1 0)
                            (vector3 fill-s2 border-t1 0)
                            (vector3 border-s2 border-t1 0)

                            (vector3 border-s1 fill-t1 0)
                            (vector3 fill-s1 fill-t1 0)
                            (vector3 fill-s2 fill-t1 0)
                            (vector3 border-s2 fill-t1 0)

                            (vector3 border-s1 fill-t2 0)
                            (vector3 fill-s1 fill-t2 0)
                            (vector3 fill-s2 fill-t2 0)
                            (vector3 border-s2 fill-t2 0)

                            (vector3 border-s1 border-t2 0)
                            (vector3 fill-s1 border-t2 0)
                            (vector3 fill-s2 border-t2 0)
                            (vector3 border-s2 border-t2 0)))))
    (with-texture texture
      (render-mesh mesh))))
