;;; Sly
;;; Copyright (C) 2013, 2014 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Font rendering.
;;
;;; Code:

(define-module (sly render font)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-4)
  #:use-module (srfi srfi-9)
  #:use-module (system foreign)
  #:use-module ((sdl2) #:prefix sdl2:)
  #:use-module ((sdl2 surface) #:prefix sdl2:)
  #:use-module ((sdl2 ttf) #:prefix sdl2:)
  #:use-module (sly render color)
  #:use-module (sly config)
  #:use-module (sly render sprite)
  #:use-module (sly render texture)
  #:export (enable-fonts
            load-font
            load-default-font
            font?
            font-point-size
            make-label))

;;;
;;; Font
;;;

(define-record-type <font>
  (make-font ttf point-size)
  font?
  (ttf font-ttf)
  (point-size font-point-size))

(define (enable-fonts)
  "Enable font rendering."
  (sdl2:ttf-init))

(define (load-font filename point-size)
  "Load the TTF font in FILENAME with the given POINT-SIZE."
  (if (file-exists? filename)
      (make-font (sdl2:load-font filename point-size) point-size)
      (error "File not found!" filename)))

(define* (load-default-font #:optional (point-size 12))
  "Load the Sly default TTF font.  POINT-SIZE is an optional
argument with a default value of 12."
  (load-font (string-append %datadir "/fonts/DejaVuSans.ttf") point-size))

(define %sdl-white (sdl2:make-color 255 255 255 255))

(define (render-text font text blended?)
  "Return a new texture with TEXT rendered using FONT."
  ;; An empty string will result in a surface value of #f, in which
  ;; case we want to abort the texture creation process.
  (let ((surface ((if blended?
                      sdl2:render-font-blended
                      sdl2:render-font-solid)
                  (font-ttf font)
                  text %sdl-white)))
    ((@@ (sly render texture) surface->texture) surface 'linear 'linear)))

(define* (make-label font text #:key (anchor 'top-left) (blended? #t))
  "Create a sprite that displays TEXT rendered using FONT.  ANCHOR
defines the sprite's origin, which is 'top-left' by default."
  (let ((texture (render-text font text blended?)))
    (make-sprite texture #:anchor anchor)))
