;;; Sly
;;; Copyright (C) 2013, 2014 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; SDL event handlers.
;;
;;; Code:

(define-module (sly event)
  #:use-module ((sdl2 events) #:prefix sdl2:)
  #:export (process-events
            register-event-handler))

(define (process-events)
  "Process all events in the input event queue."
  (let ((e (sdl2:poll-event)))
    (when e
      (handle-event e)
      (process-events))))

(define event-handlers (make-hash-table))

(define (register-event-handler event-type proc)
  (hashq-set! event-handlers event-type proc))

(define (handle-event e)
  "Run the relevant hook for the event E."
  (define (event-type e)
    (cond
     ((sdl2:keyboard-down-event? e)
      'key-down)
     ((sdl2:keyboard-up-event? e)
      'key-up)
     ((sdl2:mouse-button-down-event? e)
      'mouse-button-down)
     ((sdl2:mouse-button-up-event? e)
      'mouse-button-up)
     ((sdl2:mouse-motion-event? e)
      'mouse-motion)
     ((sdl2:joystick-button-down-event? e)
      'joy-button-down)
     ((sdl2:joystick-button-up-event? e)
      'joy-button-up)
     ((sdl2:joystick-axis-event? e)
      'joy-axis-motion)
     ((sdl2:window-resized-event? e)
      'window-resize)
     ((sdl2:quit-event? e)
      'quit)))

  (let ((handler (hashq-ref event-handlers (event-type e))))
    (and handler (handler e))))
