;;; Sly
;;; Copyright (C) 2014, 2015 David Thompson <dthompson2@worcester.edu>
;;;
;;; Sly is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Sly is distributed in the hope that it will be useful, but WITHOUT
;;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General
;;; Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Build time configuration.
;;
;;; Code:

(define-module (sly config)
  #:export (%datadir
            %sly-version
            scope-datadir))

(define %datadir
  (or (getenv "SLY_DATADIR") "@sly_datadir@/sly"))

(define %sly-version "@PACKAGE_VERSION@")

(define (scope-datadir file)
  "Append the Sly data directory to FILE."
  (string-append %datadir file))
