;;; Sly
;;; Copyright (C) 2013, 2014 David Thompson <dthompson2@worcester.edu>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Wrappers over SDL mixer.
;;
;;; Code:

(define-module (sly audio)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-2)
  #:use-module ((sdl2 mixer) #:prefix sdl2:)
  #:export (enable-audio
            load-sample
            sample?
            sample-audio
            set-sample-volume
            play-sample
            load-music
            music?
            music-audio
            music-volume
            play-music
            set-music-volume
            pause-music
            resume-music
            rewind-music
            stop-music
            music-paused?
            music-playing?))

(define (enable-audio)
  ;; The SDL mixer will throw an exception if it cannot initialize a
  ;; particular audio format.  We don't want this to be fatal, so we
  ;; ignore it.
  (false-if-exception (sdl2:mixer-init))
  (sdl2:open-audio))

;; Used to wrap SDL audio functions whose return values should be
;; ignored.
(define-syntax-rule (ignore-value body ...)
  (begin
    body ...
    *unspecified*))

;; Wrapper over SDL audio objects.
(define-record-type <sample>
  (make-sample audio)
  sample?
  (audio sample-audio))

(define (load-sample file)
  "Load audio sample from FILE or return #f if the file cannot be
loaded"
  (let ((audio (sdl2:load-chunk file)))
    (if audio (make-sample audio) #f)))

(define (set-sample-volume volume)
  "Set the volume that all samples are played at to VOLUME, an integer
value between 0 and 128."
  (ignore-value (sdl2:set-channel-volume! #f volume)))

(define (play-sample sample)
  "Play the given audio SAMPLE."
  (ignore-value
   (false-if-exception
    (sdl2:play-chunk! (sample-audio sample)))))

;; Wrapper over SDL music objects.
(define-record-type <music>
  (make-music audio)
  music?
  (audio music-audio))

(define (load-music file)
  "Load music from FILE."
  (make-music (sdl2:load-music file)))

(define (music-volume)
  "Return the volume that music is played at."
  (sdl2:music-volume))

(define (set-music-volume volume)
  "Set the volume that music is played at to VOLUME, an integer value
between 0 and 128."
  (ignore-value (sdl2:set-music-volume! volume)))

(define* (play-music music #:key loop?)
  "Play the given MUSIC."
  (sdl2:play-music! (music-audio music) (if loop? #f 1)))

(define (pause-music)
  "Pause the current music track."
  (sdl2:pause-music!))

(define (resume-music)
  "Resume the current music track."
  (sdl2:resume-music!))

(define (rewind-music)
  "Restart the current music track."
  (sdl2:rewind-music!))

(define (stop-music)
  "Stop playing the current music track."
  (sdl2:stop-music!))

(define (music-playing?)
  "Return #t if music is currently playing."
  (sdl2:music-playing?))

(define (music-paused?)
  "Return #t if music is currently paused."
  (sdl2:music-paused?))
